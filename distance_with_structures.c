//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>

struct coordinate
{
    float x;
    float y;
};
typedef struct coordinate Coordinate;
Coordinate input()
{
    Coordinate p;
    printf("Enter x cordinate :");
    scanf("%f",&p.x);
    printf("Enter y cordinate :");
    scanf("%f",&p.y);
    return p;
}
float dist(Coordinate p1,Coordinate p2)
{
    float c;
    c=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
    return c;
}

void output(Coordinate p1,Coordinate p2,float d)
{
    printf("The distance between (%.2f,%.2f) and (%.2f,%.2f) is %f",p1.x,p1.y,p2.x,p2.y,d);
}

int main()
{
    float d;
    Coordinate p1,p2;
    p1=input();
    p2=input();
    d=dist(p1,p2);
    output(p1,p2,d);
    return 0;
}