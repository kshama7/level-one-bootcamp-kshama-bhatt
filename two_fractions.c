//WAP to find the sum of two fractions.

#include<stdio.h>

typedef struct fraction
{
    int num,den;
}fraction;

fraction fetch(char arr[3])
{
    fraction f;
    
    printf("Enter the numerator of the %s fraction: ", arr);
    scanf("%d", &f.num);
    
    printf("Enter the denominator of the %s fraction: ", arr);
    scanf("%d", &f.den);
    
    return f ;
}

int gcd(int a, int b)
{
    int temp;
    
    while (b != 0) 
    {
        temp = b;
        b = a % b;
        a = temp;
    }
    return temp;
}

fraction add(fraction a, fraction b)
{
    fraction sum;
	sum.num = (a.num)*(b.den)+(b.num)*(a.den);
	sum.den = (a.den)*(b.den);

    return sum;
}

fraction simplify(fraction sum)
{
    int factor = gcd(sum.num, sum.den);
       
    sum.num = sum.num/factor;
    sum.den = sum.den/factor;
       
    return sum;
}

void print(fraction a, fraction b, fraction sum)
{
    printf("The sum of %d/%d and %d/%d is %d/%d", a.num, a.den, b.num, b.den, sum.num, sum.den);
}

int main()
{
    fraction first = fetch("1st");
    fraction second = fetch("2nd");
    
    fraction sum = add(first, second);
    
    sum = simplify(sum);
    
    print(first, second, sum);
    
    return 0;
}