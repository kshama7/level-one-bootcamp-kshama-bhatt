//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
void output(float x1, float y1, float x2, float y2, float distance);
float x1input();
float y1input();
float x2input();
float y2input();
float dist(float a,float b,float c,float d);

int main()
{
    float x1,y1,x2,y2;
    x1=x1input();
    y1=y1input();
    x2=x2input();
    y2=y2input();
    output(x1,y1,x2,y2,dist(x1,y1,x2,y2));
    
}



float x1input()
{
    float p;
    printf("Enter the abscissa for point 1: \n");
    scanf("%f",&p);
    return p;
}

float y1input()
{
    float q;
    printf("Enter the ordinate for point 1: \n");
    scanf("%f",&q);
    return q;
}
float x2input()
{
    float r;
    printf("Enter the abscissa for point 2: \n");
    scanf("%f",&r);
    return r;
}
float y2input()
{
    float s;
    printf("Enter the ordinate for point 2: \n");
    scanf("%f",&s);
    return s;
}
float dist(float a,float b,float c,float d)
{
    float dis;
    dis=sqrt(pow((c-a),2)+pow((d-b),2));
    return dis;
}



void output(float x1, float y1, float x2, float y2, float distance)
{
    printf("The distance between (%.1f,%.1f) and (%.1f,%.1f) is %.2f\n",x1,y1,x2,y2,distance);
}  