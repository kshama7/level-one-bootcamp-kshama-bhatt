//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float vol(float h ,float d, float b);
void output(float h , float d, float b, float volume);
float input(char y);

int main() 
{
	float h,d,b ;
	
	h = input('h');
	d = input('d');
	b = input('b');
	output(h,d,b,vol(h,d,b));
	return 0;
}

float vol(float h ,float d,float b)
{	
	float volume=((h*d)+d)/(3*b); 
	return volume;
}

float input(char y)
{
    float x;
    printf("Enter a value %c\n",y);
    scanf("%f",&x);
    return x;
}

void output(float h , float d, float b, float volume)
{     
	printf("Volume of tromboloid with with h = %f, d=%f and b=%f is %f",h,d,b,volume );
}


